CONTAINER ORCHESTRATION & KUBERNETES 

Deploy more complex apps under microservices architecture ( multiple containers)
--> high availability/ no downtime
--> scalability / high performance
--> disaster recovery / backup and restore 

K8s components:

1. Node -> physical or virtual server
2. Pod -> abstraction over container ( smallest unit K8s )
"Each Pod gets its own IP address
 Pods are ephemeral (can die easily) IP address recreated as new Pod recreated
"
3. Service -> permanent IP address that can be attached to Pod, also acts as load balancer between Pods
4. Ingress -> forwarding from request to Service (allows display dns in url)
5. ConfigMap -> external config of app 
6. Secret -> used to store secret data, base64 encoded
7. Volumes -> persists data 
8. Deployment -> blueprint for Pods ( number of replicas) - abstraction layer over Pods
9. StatefulSet -> Deployment for stateful apps or DBs
10. Replicaset -> abstraction layer between pods and deployments ( manages replicas of pods)
11. DaemonSet -> runs on every worker node
12. CRD - Custom Resource Definition
13. emptyDir -ephemeral volume type that is creted when a Pod is assingned to a Node, exists as long as the 
Pod is running (data safe across container crashes)

K8s arhitecture:
" Each worker node has multiple pods in it "
3 processes must be installed on every node:
 - container runtime (docker,container)
 - Kubelet - interacts with both caontainer and node
 - Kube proxy - forwards request from pods
 master nodes have 4 processes running:
 - api server - gateway & auth 
 - scheduler - schedules pods across nodes 
 - controller manager - detects stae changes (eg crashing pods) and tries recover cluster state 
 - etcd - key value store of cluster state prod cluster setup: multiple master and worker nodes.

 # Minikube - for testing cluster setup on local: master & worker procceses run on 1 node 

 Kubect 1 - cmd line tool for K8s : talk to API server
   kubect1  get nodes
   kubect1  get pod    
   kubect1  get pod -o wide (display more info for pod like IP address )
   kubect1  get deployment
   kubect1  get services
   kubect1  create deployment NAME --image=image
   kubect1  edit deployment NAME
   kubect1  logs NAME
   kubect1  describe pod NAME
   kubect1  exec -it NAME --bin/bash
   kubect1  delete deployment NAME
   kubect1  apply -f FILENAME ( takes config file and exec what is in file, can be used to create edit delete)

K8s config file 
   3 parts:
    1.metadata
    2.specification
    3.status( auto generated by K8s that checks diff to update state if applicable)
  store file w code 
  pod has own config inside config connect deployment to pods, deploment to services using lables & slectors
  need to config ports for services (targetPort = container port that services wants to connect to )

Namespaces - organise resources
    4 name spcae by default:
    - kube-system - system processes(DONT modify)
    - kube-publuic - configmap containing cluster info 
    - kube-node-lease - heartbeat of nodes for availability
    - default - resources you create if haven't specified custom namespace
create namespace options:
    1. kubect1 create namespace <NAME>
    kubect1 get namespace
    2. create with config file
    kubect1 apply -f <FILENAME> --namespace=<NAMESPACE>
    more good alocate is to include directly in config file metadata
    - group resorces in Namespaces to get better overview of resources
    - avoid conflicts between teams
    - resources sharing (dev and staging , blue/green prod)
    - access and resources limits (CPU,RAM,Storage)
considerations:
    each NS needs own configMap & secrets services can be shared across NS
    volumes & nodes can't be created withi Ns 
    can change the default Ns w KUBERNETES

Service is abstractions layer that has IP address 
Service can have multiple ports, but they have to be named in this case.
Service tyoes:
    - ClusterIP : default type 
    - Headless : set 'clusterIP:None' (no service IP given, meaning that client will talk directly to Pod IP indtead, useful when need to talk to specific Pods)
    - NodePort: accessible from outside cluster using fixed port on each Worker node(range 30.000 -32.767 is not secure)
    - LoadBalance : cluster becomes accessible using cloud providers ntive load balancer ( extension and more secure alternative to NodePort wich should only be  used for quick testing)

# Ingress #
" preferred alternative to external services routing rules to map to service name and port domain name needs
to be valid and based in IP address of node entry point need to install Ingress controller to evaliate all rules
default backend if no service mapping found -> can create service with name 'default-http-backend' to handle such
cases port 80
can define multiple paths for same host
can have multiple hosts with 1 path, each host representing a subdomain
can configure https by importing tls certificate as secret within same namespace
"

# Volumes # 
"
persist data need to decidee what type of storage is needed and manage it yourself
not namespaced, accessible from everywhere in cluster provisioned by admin 
application needs to claim volume storage via Persistent Volume Claim (pvc)- both claim and pod claiming need to be in same namespace
can create Storage Class to provision Persistent Volumes dynamically ( abstract underlying storage provide)
"
# ConfigMap & Secret volumes ( - local volume types)
 can create individual key-value pairs - as env vars
 or mount files - mount as volume types
 1) mount volume into pods
 2) mount volume into container ( mountPath, path depends on type app that is mounted)

 # StatefulSet = deploying stateful apps
  - persistend id for each pod
  - only master has own allowed to write data, all other worker pods can only read
  - each pod ahs own storage( for PV use remote storage),continously sync data between pods
  - each new pod clones data from previous up-to-date pod 
  - each pod gets own fixed DNS endpoint

# Managed Kubernets Services # 
"Cluster setup done automatically for you, just need to specify number worker nodes need ( saves effort however harder to migrate due to vendor lock-in)
out choose regions ti setup closet to user can automate provisioning of cluster as many providers integrate with tools like Terraform"
"
  _| Helm - package manager fot K8s |_
   
   Helm Charts - pkg YAML files & distribute them in public + private repos to be shared
   can find deployment w Helm
   helm search repo <keyword>
   OR browse Helm Hub
  templating engine: define coomon blueprint and replace dynamic values (useful for deployment and across enviremont)
  cmd:
    helm search repo <keyword>
    helm repo add <keyword>
    helm install <pod_name> --values <values_file> <helm_chart>
    helm uninstall <helm_chart>

  Helmfile - declare a Definition of an entire K8s cluster in single yaml file ti deploy
    brew install Helmfile
    helmfile sync
    helmfile list
    helmfile destroy

  Host Helm charts in git repo:
    # with app code or no separate git repo for helm charts

  Pull from private Docker repo 
    1. Create secret component w creds for Docker registery - type dockerconfigjson
      kubect1 create secret docker-registery <secret_name> --docker-server=<registery_url> --docker-username=<username> --docker-password=<password>
       (only use above cmd for 1 secret, otherwise use 2-step docker login + secret config file)
    2. Config deployment to use secret - imagePullPolicy: Always secret must be in same namespace as the deploment

    _| Operators |_
      Mainly for stateful apps custom control loop mechanism custom K8s components 
      automatically manage full lifecycle of stateful apps those with domain specific knowledge create operator for that purpose

  # Authorization & RBAC (Role Based Access Control) #

  "For Dev restrict access to only specific namespace - RBAC via Role component & RoleBinding component Role
  defines resources and access permissions RoleBinding (link Role to User or Group) For admins restrict access to cluster-wide ops = RBAC via ClusterRole & ClusterRoleBinding components
  K8s relies on externa; sources for auth (static token file , certificate, 3rd party id service like LDAP)via API server
  for APP Users can use ServiceAccount (component that reprsent Srvice user) can link Service Account to Role via Rolebinding or ClusterRole via ClusterRoleBinding
  'auth can-i' -kubect1 cmd to check permissions for current user."

 # Microservices in K8s # 
 "microservices COMMUNICATE VIA api CALLS OR MESSAGES-BASED COMMUNICATIONS popular
  arhitecture currently is Service Mesh archiyecture - each microservices has own helper app (ie.SideCar container)
  that help communication between server.
 "
  Useful info as DevOps:
    - what microservices are being deployment
    - how they are connected? (Useful to create a connector graph)
    - any 3rd party services or databsees?
    - wich Service is accessible fro outside the cluster?
    - image names for each microservices
    - envs for each microservices
    - on wich port each microservices starts
    - namespace?

  Best practice K8s
    - specify pinned version for each image
    - configure livenessProbe on each container ro check if app is healty
    - config readinessProbe on each container to check if app has syarted up successfully (exec,tcpSocket, HTTP probes)
    - config resorces request for each container (cpu, memory)
    - comfig resource limits
    - don't expose a NodePort ( as exposes entrypoint on each worker node) - best to use LoadBalancer p ingress, that creates single entrypoint to cluster
    - config more thant 1 worker node in cluster
    - use labels for all resorces
    - group resources in Namespaces
    - ensure images are free of vulnerabilities (perform vulnerability scans )
    - no root access for containers 
    - update K8s to latest version (best to do node ny node to avoid app downtime )