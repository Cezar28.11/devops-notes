AWS SERVICES 

Examples of services provided by AWS
    EC2 - virtual servers
    S3 - storage
    VPC - network
    IAM - users & permissions 
    ECR - container storage
    ECS - container orchestraction
    EKS - container orchestraction w Kubernets

Services scopes:
    1. global - eg. IAM 
    2. region - eg. S3, VPC 
    3. AZ (availabality Zone ) - eg. EC2

IAM - Identity Access Management
 --> Create by default for each region
 --> Best practice to create admin user with less privileges than root user (root created bt deafault when creating AWS account)
 --> This admin user can then create other human users & system users
 --> groups can be created
 --> Unlike IAM users,policies can't be set directly for AWS services - need to create IAM role first and
 then attach the policy to the IAM role 

REGION and AZ 
    Region = physical regions where data centers are cluster
    Availabality Zone = 1 or more discrete data centers - used for replication/ backups

VPC - Virtual Private Cloud 
--> Created by default for each region
own isolatted network in cloud spans all AZ (Subnet) in that Region
Subnet for each AZ - private network inside network
--> Firewall Rule config make Subnet either private or public internal IP range for VPC - for comm inside VPC,nor external
traffic.Subnet then gets su IP range based on VPC'c internal IP range also get public IP address
--> Internet Gateway connects VPC to outside internet NACL (NETWORK ACCESS CONRTROL LIST) config access on subent level
Security Group config access on instance level.

CIDR Blocks
 Range of IP addresses lower number /xx - then higher the range 
 VPC has CIDR block - Subnet sub CIDR block.

EC2 - virtual server in AWS cloud ( Demo project 6)

# AWS CLI # 
--> install aws cli -> brew install awscli 
--> config aws cli to connect to aws account (privide aws acces key ID, and secret acces key,region,output format)-->aws configure
--> cmd structure --> aws <cmd> <sub-cmd> - options 

# EC2 cmd # 

    aws ec2 run-instance --image-id xxxx -- count x ...
    aws ec2 decribe-vpcs 
    aws ec2 create-security-group <attr>
    aws ec2 authorize-security-group-ingress <attr>
    aws ec2 create-key-pair <attr>
    aws ec2 describe-subnets
can Filter & Query services describe cmd 
IAM cmd 
    aws iam create-group <attr>
    aws iam create-user <attr>
    aws iam add-user-to-group <attr>
    aws iam get-user <attr>
    aws iam get-group <attr>
    aws iam list-policies <attr>
    aws iam attach-user-policy <attr>
    aws iam attach-group-policy <attr>
    aws iam list-attached-group-policies <attr>
    aws iam create-login-profile <attr>
    aws iam create-policy <attr>
    aws iam create-access-key <attr>
change user for aws cmd 
    aws configure to change default User 
    aws configure set aws_access_key_id <value>
    OR could just set env var tot not persist config changes
    export AWS_ACCESS_KEY_ID=value 
    export AWS_SECRET_ACCESS_KEY=value
delete aws resources
    aws <service> <delete_cmd>




