git clone' 
(create local copy of remote repo)

git add <files> (add files to the staging area )

git add . (add all files )

git reset ( undo the changes to local files restore the last commit)

git diff (display the difference between files in two comm or between a comm and your current
repo)

git command

git log ( it is used to view the entire commit history)

git push

git pull <remote> ( used to pull down all the updates from the remote repository )

git pull ( used to fetch and merge ay commits from the tracking remote branch)

git init (initializes a new git repository)
git remote add origin <remote_repo>
git push --set-upstream origin master
(create local git repo and connect to remote repo)

git checkout <branch>

git checkout -b <branch>
(create new branch locally)

git checkout-b <branch-name> (create a new branch and switches to the new one )


git status (used to check state of staging area and the working directory)

git branch -d <branch>

best practice:
1 branch per feature
dev branch: intermediary master branch 
pull/merge requests 
delete branch when merged
add .gitignore file 

git rebase 
(avoid unnecessary merge commits in git history)

git rm -r - -cached <folder>
git rm - - cached <file>
(remove file frome remote repo)

git stash
git stash pop
(save unfinished changes)

each commit has a unique hash
git checkout <commit_hash>(can be useful for testing/reproducing bugs)
('detached'HEAD state: not latest state)

If not pushed to remote repo yet 
git reset --hard HEAD~1
(revert & keep changes)
git commit --amend 
(merge changes into last commit)
If arleady pushed to remote repo(only when working alone in branch)
    git reset --hard HEAD~1
    git push -- force

git revert <commit_hash>
(creates new commit to revert old commit changes)

git merge 

remove local.git file (contains config for git remotes etc)
rm -fr .git

GIT for DevOps
infrastucture as code 
CI/CD Pipeline & build automation