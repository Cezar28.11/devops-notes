OS & LINUX 

Operating system(OS) = middlem between apps and hardware
Server several roles:
Traslator
Manages resources among apps(CPU/process,memory,storage,file system,I/O devices eg.mouse,)

OS components:
Kernel(layer between apps & hardware) which is a program made of drivers & logic
App layer

Client OS vs Server OS (No GUI or I/O devices so more lightweight)

Virtualization & Virtal Machines
Hypervision to run multiple Virtual Machines using a physical Host OS
VMs are isolated
Type 2 VS Type 1 Hypervision:Type 1 installed directly on hardware for serves,Type 2 users host OS on personal computer
Benefits Type 2: learn & experoment, don't endanger main OS,test app on different OS
Benefits Type 1: efficient usage of hardware resources,abstraction of OS from hardware via VMI with backups/snaphots.

#LINUX File System.
Everything in Linux is a file 
1 root folder / root user home dir 
/home dir of non-root user 
/bin executables for essential system cmd 
/sbin system binares,need super user privileges to execute
/lib shared lib that execs from/bin or / sbin use 
/usr was used fir user home dir (historic reasons due to storage limitations )
/usr/local program that install on computer(3rd party pages) avaible for all users
/opt 3 rd party programs you install that DONT split its components
/boot files required for booting
/etc system config
/dev devices files(wecam,mouse,keybord etc)
/var stores logs
/var/cache
/tmp temporary resources required for processes
/media removable media
/mnt temporary mount points
Hidden files(starts with a dot)

Linux cmd
username@hostname:-$
($ regular user, # super user )
ls = list contents
cd = change dir (cd / =change to root dir)
mkdir = make dir 
touch = create file 
rm = delete file
rm -r = delete non-empty dir with files in it 
rmdir - delete empty dir
clear = clears terminal
mv <old-name> <new-name> = rename file to new name
cp -r <dir> < new-dir > = copy contents folder to new folderls
ls -R = list all folders and files in each 
history = lists all recent cmd in terminal
ls - a = display Hidden files
ls - l = print files in long list format(ls -la for listing Hidden files)
cat = show contents files
lscpu = cpu info
lsmem = memory info
sudo = grants super user privileges for cmd
su -<username> = switch user 
| = pipe,passes output of one cmd as input of next cmd 
<input> | less = display reader friendly format for info in CLI
<input> | grep <pattern> = filter input based on pattern search
> = redirect,takes output from previous cmd and sends it to file that you give(overrides contents file)
>> = appends text to end files 
Can pass multile cmd in one line separated by

PACKAGE MANAGER:APT
Resolves dependencies for installling software
Ensure integrity & authemcity of PACKAGE
Donwloads,installes ou updates existig software from repo
Knoes wher to put files in system

apt search < package name >
apt install < package name >
apt remove < package name >
apt update

VIM text editor 
:wq quit and save 
:q! quit without save 
dd delete line 
d10d delete next 10 lines
u undo
A jump to end of line, switch insert mode
$ jump to end line,without switch insert mode
0 kump to beginning of line 
12G jump to line 12
/pattern search for pattern
  n jump to next match
  N jump to previous match

:%s/old/new/ replace old with new throught file

Users,Groups & Permissions 
3 user categories:
Root user 
Regular userService user(best practice for security)

Can Group users and define group permissions 
Users can have multiple groups

adduser <username>
password <username>
su - <username>
su - switch root user 
groupadd <groupname>
deluser <username>
groupdel <groupname>
usermod [OPTIONS] <username>
usermod -g <groupname> <username>
usermod -G <groupname> <username> (overrides secondary groups list)
usermod -aG <groupname> <username> (appends to existing list)
gpasswd -d <username> <groupname>
exit (logout user)

chown <username>:<groupname> <filename> (change file ownership)
chgrp <groupname> <filename>

# Example permission info for file:dewxewxr-x #

d directory
-normal file

(owner)
r reade
w write
x execute
-No permission

chmod -x <filename> (remove execute permission for all owners)
chmod g-x <filename> (remove write permission for group )
chmod g+x <filename> (add execute permission for group)
chmod u+x <filename> ( add execute permission for group)
chmod o+x <filename> ( add execute permission for group)
chmod g=rwx <filename> (sets specific block permission for group)
chmod 777 <filename> ( gives all permission to all owner)

# Shell scripting #
.sh file extension
Bash (Bourne-again Shell): shell program
Shebang: tells OS which shell program to use eg. #!/bin/bash

# Variables #
Declare variables: <variablename> = <value>
Can insert cmd into variables using $() eg. files=$(ls files)
Use variables:$<variablesname>

#Conditionals:
if[condition]
then
  statement
elif[condition]
then
  statement
else
  statement
fi

# OPERATORS # 
File OPERATORS
Eg. -d <name> (test if dir exists with that name )
Num OPERATORS:
Eg. <numbervalue> -eq <number> (checks equality)
String Operators:
<value> == <value> (checks equality)

# PARAMETER # 
Pass parameter from outside script = <variablename1>=$1 <variablename2>=$2
Rhen reference in script exec cmd = <scriptname> <param1> <param2>
Read user input = read -p <variableinputname>
$* = list all params provided
$# = counts number params

# LOOPS # 
For loops: iterating over list 
for <el> in <list>
  do 
done
While loops: exec cmd repeatedly until condition matched
while <condition>
  do 
done 
break = cmd to exit loop
$(( <aritmetic operations> ))

# FUNCTIONS # 
define = function <functionname>(){
  <param1>=$1
  [logic $<param1>]
  return <variablename>
}
execute = <functionname>
$? = captures value returned by last cmd 

Comment out line = # 

# ENV Variables # 
Available for whole envirment
Defined upper case 
printenv = prints all new variables
printenv <ENV> = prints specific env var
export <ENV> =<value>= define env var
unset <ENV> = delete env var
To persist env var for specific user, add item .bashrc file ,then run: osurce .bashrc
For system wide env var definition,add to PATH variable(lists all file paths in wich bin cmd get executed)located in /ect/environment
PATH=$PATH:<userhomedir> = acces env var anywhere for specific user env without providing actual path each time 
 

# NETWORKING # 
LAN: local area NETWORKING
IP: internet protocol (32 bit value;bit = 0 or 1 )
Switch : sits within LAN
Router: sits between LAN and outside network (WAN)
Gateway: IP address of Router
Subnet: IP adress range( uses subnet mask to fixate bits of IP address range)
NAT: Networj address Translation,private IP address of device within LAN is replaced by Router
(security and reusability of IP addresses)
Firewall: prevent unauthorized access from entering private network using firewall rules,allows device IP address at porty to be accessed(port forwarding)
Port : every device has set of ports(doors throughtwich can access)
DNS : Doman Name Service: mapping IP addressesto names
Every device has DNS clients to perform mapping
Domains $ subdomains
# Useful cmd:
ifconfig: get computer networking info
netstat: check active connections on device
ps aux :check current running process
nslookup : check any domain name IP address
ping: check if service is accessible 

# SSH # 
Secure shell : way to authenticate securely over web
SSH key pair (private + public keys)
Allow port 22 in firewall rule for restricted IP address
ss-keygen -r rsa
scp = securely copy files and dir (encrypted)


# EXTRA LEARNINGS # 

grep "string1\string2"
finds a line in the output that has "string1" or "string2" in it 

>awk {print substr($ 3,2,2)}
takes a line and grabs the third section of the string and from there grabs the first 2 characters

>echo-n
message to tell echi not to append a newline 

>read
bash builtin command that reads the contents of a line into a variable

>head
used to print the first ten lines(bt default) or any other amount specified of a file or files

>mkdir -p
create sub-directories of a directory.It will create parent directory first,if it doesn't existig
But if it arleady exist,the it will not print an error message and will move further to create sub-directories

>grep -v
matches only those lines that do not contain the given word

>runuser
changes the user that runs a command
only the root user can call runner